import { Router, Request, Response, NextFunction } from 'express'
import { MongooseError } from 'mongoose'
import createNetworkTrace from '../mongo/network_trace/create'
import express from 'express'
import createClientTrace from '../mongo/network_trace/create'
import { ClientTrace } from '../types/Network'
// import type { ClientNetworkTrace } from '../types/Network'

const network_traces_list = function(req:Request, res:Response):void {
  res.send('NOT IMPLEMENTED: Author list')
}

const network_trace_by_id = function(req:Request, res:Response):void {
  res.send('NOT IMPLEMENTED: Author detail: ' )
}

const network_trace_create_get = function(req:Request, res:Response):void {
  res.json({get: 200})
}

const network_trace_create_post = function(req:Request, res:Response):void {
  const trace:ClientTrace = req.body
  createClientTrace(trace)
  res.json({post: 200})
}

const network_trace_delete_get = function(req:Request, res:Response):void {
  res.send('NOT IMPLEMENTED: Author delete GET')
}

const network_trace_delete_post = function(req:Request, res:Response):void {
  res.send('NOT IMPLEMENTED: Author delete POST')
}

const network_trace_update_get = function(req:Request, res:Response):void {
  res.send('NOT IMPLEMENTED: Author update GET')
}

const network_trace_update_post = function(req:Request, res:Response):void {
  res.send('NOT IMPLEMENTED: Author update POST')
}

export default function NetworkController(): Router {
  const router = Router()

  router.get('/', network_traces_list)

  router.get('/show/:id', network_trace_by_id)

  router.get('/create', network_trace_create_get)

  router.post('/create', network_trace_create_post)

  router.get('/delete', network_trace_delete_get)

  router.post('/delete', network_trace_delete_post)

  router.get('/update', network_trace_update_get)

  router.post('/update', network_trace_update_post)

  return router
}
  