import type { NetworkState, useMemory } from '@vueuse/core'

export interface InternetProtocolState {
  ipv4: string|null
  ipv6: string|null
}

export interface GeoStateAS {
  asn: string|null,
  domain: string|null,
  name: string|null,
  route: string|null,
  type: string|null
}

export interface GeoStateLocation {
  city: string|null,
  country: string|null,
  geonameId: number|null,
  lat: number|null
  lng: number|null
  postalCode: string|null,
  region: string|null,
  timezone: string|null
}

export interface GeoStateProxy {
  proxy: boolean|null
  tor: boolean|null
  vpn: false
}

export interface GeoState {
  as: GeoStateAS,
  ip: string|null,
  isp: string|null,
  location: GeoStateLocation
}

export interface ClientTrace {
  memory: typeof useMemory[]|null
  network: NetworkState[]|null
  internetProtocol: InternetProtocolState[]|null
  geo: GeoState|null
}

export enum Topic {
  work = 'work-with-me',
  feat = 'collaborate-together',
  other = 'other',
  bug = 'bug-issue'
}

export interface Mail {
  address: string
  subject: string
  content: string
  topic: Topic
}

export interface GuestData {
  name: string
  messages: Mail[]
}
