"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
//import bodyParser from 'body-parser'
const dotenv_1 = __importDefault(require("dotenv"));
// eslint-disable-next-line @typescript-eslint/no-var-requires
const cors = require('cors');
const path_1 = __importDefault(require("path"));
const network_controller_1 = __importDefault(require("./controllers/network_controller"));
dotenv_1.default.config();
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: false }));
app.use(cors());
app.use(express_1.default.static(path_1.default.join(__dirname, 'public')));
app.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.json({ ExpressJS: 'on' });
}));
app.use('/network_trace', (0, network_controller_1.default)());
app.listen(3200, () => {
    console.log('⚡️[server]: http://localhost:3200');
});
