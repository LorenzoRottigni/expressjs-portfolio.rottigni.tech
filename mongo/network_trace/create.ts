import { Schema, model, connect } from 'mongoose'

import { ClientTrace } from '../../types/Network'

const networkTraceSchema = new Schema<ClientTrace>({
  memory: { type: [Object], required: false},
  network: { type: [Object], required: false},
  internetProtocol: { type: [Object], required: false},
  geo: { type: [Object], required: false }
})

const NetworkTraceModel = model<ClientTrace>('NetworkTrace', networkTraceSchema)

export default async function run(payload:ClientTrace) {
  console.log('preconnect')
  const connection = await connect('mongodb://root:root-secret-password@10.70.70.120:27017/portfolio_rottigni_tech?connectTimeoutMS=1000')
  console.dir(connection)
  console.log('connected')
  const networkTrace = new NetworkTraceModel(payload)
  await networkTrace.save()
  console.dir(networkTrace)
}