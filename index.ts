import express, { Express, Request, Response } from 'express'
//import bodyParser from 'body-parser'
import dotenv from 'dotenv'
// eslint-disable-next-line @typescript-eslint/no-var-requires
const cors = require('cors')

import path from 'path'
import NetworkController from './controllers/network_controller'

dotenv.config()

const app: Express = express()
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors())
app.use(express.static(path.join(__dirname, 'public')))
app.get('/', async (req: Request, res: Response) => {
  res.json({ExpressJS: 'on'})
})

app.use('/network_trace', NetworkController())

app.listen(3200, () => {
  console.log('⚡️[server]: http://localhost:3200')
})
