FROM node:17-alpine

RUN mkdir -p /usr/src/express-backend
WORKDIR /usr/src/express-backend
COPY . /usr/src/express-backend/

RUN yarn cache clean --mirror
RUN yarn
RUN yarn build

EXPOSE 3200

CMD [ "yarn", "start" ]
